let command = document.querySelector(".command");

if (command) {
let left_btn = command.querySelector(".command-head__left");
let right_btn = command.querySelector(".command-head__right");
let photo = command.querySelector(".photo");
let photo_list = command.querySelector(".photo__list");
let photo_item = photo_list.querySelector(".photo__item");

let position = 0;
left_btn.addEventListener("click", turn_left_photo);
right_btn.addEventListener("click", turn_right_photo);
window.addEventListener("resize", turn, false);

function turn_left_photo(){ 
    turn(-1); 
}
function turn_right_photo( ) { 
    turn(+1);
}
function turn(direction) {
    //На сколько сместисть лист с фото
    let how_long = Number( getComputedStyle(photo_item).width      .replace(/px$/g, '') )
                 + Number( getComputedStyle(photo_item).marginRight.replace(/px$/g, '') );
    //Длина photo_list
    let list_lenght = -photo_list.scrollWidth + photo.clientWidth;
    if (event.type == "resize") {
        position = 0
    } else {
    if (direction === 1) {
        position -= how_long;
    } else {
        position += how_long; 
    }
    // Величина погрешности слайдера
    let mistake = 35;
    if (position > (0 - mistake)) position = 0;
    if (position < (list_lenght + mistake)) position = list_lenght;
    }

    photo_list.style.transform = `translateX(${position}px)`; 
}
}