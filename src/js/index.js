//style
import "../less/main.less";
//JS files 
import "./base";
import "./background";
import "./application";
import "./404";
import "./command"; 
import "./service"; 
 