let service = document.querySelector(".service");
if (service) {
    let service_list = service.querySelector(".service-list");
    let service_list_item = service_list.querySelectorAll('.service-list__item'); 

    let descriptions = service.querySelectorAll(".service-description");
  
    for (const link of service_list_item) {
        link.addEventListener("click", change_description);
    }

    function change_description() { 
        let number = this.dataset.num;

        for (const elem of descriptions) {
            elem.classList.remove("active");
        }
        for (const elem of service_list_item) {
            console.log(elem.classList.contains("active"));
            elem.classList.remove("active");
        }

        this.classList.add("active");
        descriptions[number].classList.add("active")
    }
}