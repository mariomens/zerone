//Reset text selection

document.body.onselectstart = function () {
  return false;
};
document.body.onselectstart = function () {
  return false;
};

//-/Reset text selection
//burger
//-----Node
let header = document.querySelector(".header");
let burger_menu = header.querySelector(".burger-menu");
let burger_icon = header.querySelector(".burger-icon");
let footer__socials = document.querySelector("footer").querySelector(".socials");
//-----Variable
let burger_flag = false;

//-----Script
burger_icon.addEventListener("click", burger_toggler);

burger_menu.addEventListener("transitionend", () => {
  if (!burger_flag) burger_menu.style.cssText = "display: none";
});

function burger_toggler() {
  if (!burger_flag) {
    burger_menu.style.cssText = "display: block";
    setTimeout(() => toggle(), 50);
  } else {
    toggle();
  }
}

function toggle() {
  burger_icon.classList.toggle("active");
  header.classList.toggle("active");
  footer__socials.classList.toggle("active");

  burger_flag = burger_icon.classList.contains("active");
}

//-/burger
