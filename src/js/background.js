//-----Node
let portfolio__list = document.querySelector(".portfolio__list");
if (portfolio__list != null) {
  let portfolio__items = portfolio__list.children;
  let backgroung = document.querySelector(".backgroung-img");
  //-----Variable
  
  let actual_number; 
  let touchstartX = 0;
  let touchstartY = 0;
  let touchendX = 0;
  let touchendY = 0;
  const touch_differnt = 100;
  //-----Script
  add_event();

  function change_background(event) {

    let wheel = event.type == "wheel";
    let touch = event.type == "touchend";
    let click = event.type == "click";
    let keypress = event.type == "keypress";
    let old_number;

    if (wheel) {
      ////// on wheel js
      old_number = wheel_preprocessing();
    } else if (touch) {
      //// touch preprocessing
      old_number = touch_preprocessing(event);
      console.log("New value" + old_number);
    } else if (click || keypress) {
      /////// on click js
      old_number = click_preprocessing();
    } else if ((old_number == undefined) || (old_number == null) || (old_number == defined)) 
    {return;} else {return;}

     
    if (old_number == actual_number) return;

    remove_event();

    portfolio__items[old_number].classList.remove("active"); 
    portfolio__items[actual_number].classList.add("active");

    if (old_number > actual_number) {
      put_bottom();
    } else {
      put_top();
    }
  }

  function wheel_preprocessing() {
     old_number = find_old();
 
    actual_number = old_number - Math.sign(event.wheelDeltaY);
    // Mozilla debug
    if (event.wheelDelta == undefined || event.wheelDelta == null) {
      actual_number = old_number - Math.sign(event.deltaY);
    } 

    if (actual_number > portfolio__list.children.length - 1) actual_number = 0;
    if (actual_number < 0) actual_number = portfolio__list.children.length - 1;
    // console.log(`actual_number = ${actual_number}  `);
    return old_number;
  }

  function click_preprocessing() {
    if (event.target == portfolio__list) return false;
    if (event.target == this) return false; //Что бы не сработало на portfolio__list
    actual_number = Number(event.target.dataset.slide);
    let old_number = find_old();
    return old_number;
  }
  function touch_preprocessing(event) {
    let old_number1 = find_old();

    touchendY = event.changedTouches[0].screenY;
    let differnce = touchstartY - touchendY;

    if (differnce > 0) {actual_number = old_number1 + 1} ;

    if (differnce < 0) {actual_number = old_number1 - 1} ;

    if (Math.abs(differnce) < touch_differnt) return undefined;

  console.log(Math.abs(differnce));
// Поработать с областью видимости переменных

    if (actual_number > portfolio__list.children.length - 1) actual_number = 0;
    if (actual_number < 0) actual_number = portfolio__list.children.length - 1;

    return old_number1;
  }

  function find_old() {
    let old_number2;
    for (const child of portfolio__items) {
      let active = child.classList.contains("active");
      if (active) {
 //       console.log(child.dataset.slide);
      old_number2 = Number(child.dataset.slide);
      }
    }
    return old_number2;
  }

  // work with image
  function put_bottom() {
    put(-1);
  }
  function put_top() {
    put(+1);
  }

  function put(direction_bool) {
    let img = backgroung.children;

    transition_none(img[old_number], true);
    transition_none(img[actual_number], true);

    img[actual_number].style.cssText = `top: calc( ${direction_bool}*100% )`;
    img[actual_number].classList.add("active");

    setTimeout(() => {
      img[actual_number].style.cssText = `top: calc( 0% )`;
      img[old_number].style.cssText = `top: calc( ${direction_bool}*-1*100% )`;

      img[actual_number].addEventListener("transitionend", transitioned);
    }, 100); // задержка для слабых ПК

    function transitioned() {
      img[old_number].classList.remove("active");
      style_clear(img[old_number]);
      style_clear(img[actual_number]);
      transition_none(img[old_number], false);
      transition_none(img[actual_number], false);

      add_event();
      img[actual_number].removeEventListener("transitionend", transitioned);
      img[old_number].classList.remove("active");
    }
  }

  function transition_none(img, action_type) {
    // transitionend fixer. На этом элементе срабатывало событие транзитонэнд

    let link = img.querySelector(".portfolio__all-protfolio");
    if (link === null) return;

    if (action_type) {
      link.style.cssText = "transition: none";
    } else {
      style_clear(link);
    }
  }

  function style_clear(obj) {
    obj.style.cssText = ``;
  }

  // work with courusel event

  function remove_event() {
    portfolio__list.removeEventListener("keypress", change_background, true);
    portfolio__list.removeEventListener("click", change_background, true);
    window.removeEventListener("wheel", change_background, true);
    // Для мобильный устройств добавляется событие свайп вверх и вниз
    document.removeEventListener("touchend", change_background, true);
    document.removeEventListener("touchstart", touch_start, false);
  }

  function add_event() {
    portfolio__list.addEventListener("click", change_background, true);
    portfolio__list.addEventListener("keypress", change_background, true);
    window.addEventListener("wheel", change_background, true);
    // Для мобильный устройств добавляется событие свайп вверх и вниз
    document.addEventListener("touchend", change_background, true);
    document.addEventListener("touchstart", touch_start, false);
  }

  function touch_start(event) {
    touchstartX = event.changedTouches[0].screenX;
    touchstartY = event.changedTouches[0].screenY;
  }
  ////// po
}
