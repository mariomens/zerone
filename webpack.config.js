const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin'); 

module.exports = {
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    main: "./js/index.js",
    // img: './img/',
  },
  output: {
    filename: "[name].[contenthash].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  resolve: {
    extensions: [".js", ".json", ".png", "svg", "gif", "jpeg", "jpg", "cur", "pptx"],
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@models": path.resolve(__dirname, "src/models"),
    },
  },
  devServer: {
    port: 4200,
  },
  plugins: [
    new HTMLWebpackPlugin({ 
      template: "./index.html",
    }),
    new HTMLWebpackPlugin({ 
      filename: "application.html",
      template: "./application.html",
    }),
    new HTMLWebpackPlugin({ 
      filename: "404.html",
      template: "./404.html",
    }),
    new HTMLWebpackPlugin({
      filename: "contacts.html",
      template: "./contacts.html",
    }),
    new HTMLWebpackPlugin({
      filename: "service.html",
      template: "./service.html",
    }),
    new HTMLWebpackPlugin({
      filename: "command.html",
      template: "./command.html",
    }),
    new HTMLWebpackPlugin({
      filename: "case_league.html",
      template: "./case_league.html",
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash].bundle.css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: "img", to: "img" },
        { from: "404.html", to: "404.html" },
      ],
    }),
    new FileManagerPlugin({
      onEnd: {
        copy:    [
          { source: "./files/contacts", destination: "../dist/files/contacts" },
          { source: ".htaccess", destination: "../dist/" },       ]
       }     
    }), 
  ],
  module: {
    rules: [ 
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env']
            }
        }
    },

      {
        test: /\.html$/,
        include: path.resolve(__dirname, "src/template"),
        use: ["html-loader"],
      },
/*
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader","postcss-loader"],
      },
      */
      {
        test: /\.less$/,
       /* exclude: /node_modules/, */
        use: [MiniCssExtractPlugin.loader, "css-loader","postcss-loader",   "less-loader"],
      },
      {
        test: /\.(png|svg|gif|jpg|jpeg|cur|pptx)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
    ],
  },
};
